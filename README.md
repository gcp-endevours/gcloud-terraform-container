# gcloud-terraform-container

A Dockerfile that creates a container with gcloud and terraform installed.

### Versions

| **Application** | **Version** |
| --------------- | ----------- |
| Terraform | 0.11.13 |
| gcloud | 239.0.0 |
| alpine | 3.9 |

### Usage

Build the image:
```bash
docker build -t gcloud-terraform .
```

Create a gcloud-config container for credentials, by running:
```bash
docker run -ti --name gcloud-config google/cloud-sdk gcloud auth login
```

Then run:
```bash
docker run --rm -it -v (pwd):/current-dir -w="/current-dir" --volumes-from gcloud-config gcloud-terraform
```

Additionally, add the following as an alias:
```bash
alias gcloud-terra="docker run --rm -it -v (pwd):/current-dir -w="/current-dir" --volumes-from gcloud-config gcloud-terraform"
```
